# Suckless patches
These patches are modifications to add new functionality to [suckless programs](https://suckless.org/) that use as part of my everyday tools.

# dwm
dwm is a window manager that automatically arranges windows in tiles among other useful features.

- `cycletag.diff` This patch allows to cycle through different tags (a.k.a. workspaces) using keyboard shortcuts or mouse wheel.
- `togglefullscreen.diff` This patch allows to toggle full screen for any window.
- `getkb` this is not a patch but a shell script that generates a table of key-bindings from the source code configuration file (`config.def.h`). A comment of the form `//kb <description...>` or `//mb <description...>` must bee added on the same line that contains the keyboard or mouse keybinding, respectively.
